## Sistema examen BBP
Éste sistema está escrito en NodeJS versión >= 7.10.
La arquitectura de la aplicación es propia. Bajo licencia pública.

**pre-requisitos**
* Instalar NodeJS Version v6.10.3 or higher.

### Como instalar
#### Windows

[Instalar NodeJS en Windows](https://nodejs.org/en/)


#### OsX

[Instalar NodeJS en OsX](http://blog.teamtreehouse.com/install-node-js-npm-mac)


#### Linux (Probado en Ubuntu 16.10 LTS)

[Instalar NodeJS en Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04)


##### Instalar y Generar dependencias

Una vez que tenemos NodeJS ejecutandose , instalamos las dependencias de la siguiente forma:

```
cd project-directory/
npm install

```

Y por ultimo ejecutamos la aplicacion de la siguiente forma: 

npm start  

Esto ejecutara la aplicacion haciendo uso del puerto 8000, en caso de no tener disponible dicho puerto ejectar la aplicacion de la siguiente forma:

cd bin/

node server.js --port ?  

en donde ? representa cualquier # puerto valido, previamente verificando que no este en uso.






