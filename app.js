global.debug = false;
process.argv.forEach(function (val, index, array) {
    if (val === '--pro'){
        global.production = true;
    }
    if (val === '--debug'){
        global.production = false;
        global.debug = true;
    }
});

let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let app = express();
let compression = require('compression');
app.use(cookieParser("psyban.2016"));

let dirNames = [];
dirNames['views'] = __dirname + '/views';
dirNames['public'] = __dirname + '/public';
dirNames['routes'] = __dirname + '/routes';
dirNames['locale'] = __dirname + '/public/locale';


app.use(function(req,res,next){
    res.header('X-Powered-By', "Psyban");
    next();
});

app.use(compression({ threshold: 0 }));
app.use(express.static(dirNames['public']));
app.set('views',dirNames['views']);
app.set('view engine', 'ejs');
app.use(favicon(path.join(dirNames['public'], 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


let fs          =   require("fs");
let routes  =   fs.readdirSync(dirNames['routes']);
let pRoutes = [];

app.get('/*',function(req,res,next){
    res.header('Cache-Control', 'no-cache' );
    next(); 
});

// Process to getting paths 
routes.forEach(function(val){
  val = val.replace('.ejs','');
  pRoutes[val] = require(dirNames['routes'] + "/" + val);
  app.use(pRoutes[val].route, pRoutes[val].router);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;

    console.log(req.originalUrl, " error 404", err);

    next(err);

});

// development error handler

if (global.debug) {
    console.log("debug");
    app.use(function(err, req, res, next) {
        let locale = null;
        try {
          let localeCode = req.headers["accept-language"].split(',')[0].substr(0,2);
          locale = require(dirNames['locale'] + localeCode);
        }catch(innerEx){
          locale = require(dirNames['locale']+"/es");
        }

        res.status(err.status || 500);
        locale.message = err.message;
        locale.error = err;
        res.render('error', locale);
    });
}

module.exports = app;

