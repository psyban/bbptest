let express = require('express');
let localeUtils = require('../public/locale/utils');
let router = express.Router();

router.get('/', function(req, res) {
    let locale = localeUtils.getLocale(req,res);
    try {
        res.render('home', locale);

    } catch (ex) {
        console.error(ex, req.params.view, "in catch");
        res.render("error", locale);
    }
});


module.exports = {route:"/",router:router};
