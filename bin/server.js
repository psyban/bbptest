let cluster = require('cluster');

if(cluster.isMaster) {
    let numWorkers = 1; // require('os').cpus().length;

    for(let i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
    });

    cluster.on('exit', function(worker, code, signal) {
        cluster.fork();
    });
} else {
    let app = require(__dirname + '/www');
    let port = 8000;

    process.argv.forEach(function (val, index, array) {
        if (val === '--port') {
            port = array[index+1];
        }
    });

    let server = app.listen(port, function() {
        console.log('Process ' + process.pid + ' is listening to all incoming requests in port ' + port );
    });
   
}
