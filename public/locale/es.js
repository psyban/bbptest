if (!global) {
    let global = {};
    global.debug = false;
}
let locale = {
    
    debug: global.debug,
    locale : "es",
    lang : "es",
    langs: ["es","en"],

    home:{
        title: "Control de cobros",
        cardLoadFile: "Cargar reporte de trabajo",
        cardContentFile: "Contenido del reporte",
        cardResult: "Resultados"
    },

    errors:{
        codeError:  "Código de error: ",
        notFound: "La página no ha sido encontrada"
    },

};
if (typeof module !== "undefined") {
    module.exports = locale;
}
