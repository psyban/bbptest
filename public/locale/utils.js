module.exports = {
    getLocale: function(req,res) {
        var locale = {
            dynamic: null,
            backup: null
        };
        // this is ready to support another language
        // we can get the language prefix from cookies or localStorage if it was saved before
        var localeCode =  'es';
        try {
            if (!locale.backup)
                locale.backup = require("./" + localeCode);
            locale.dynamic = Object.assign({}, locale.backup);
        } catch (innerEx) {
            console.warn("Error:", innerEx);
            if (!locale.backup)
                locale.backup = require("./es");
            locale.dynamic = Object.assign({}, locale.backup);
            localeCode = "es";
        }
        locale.dynamic.locale = localeCode;
        return locale.dynamic;
    }
};