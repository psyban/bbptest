(function(app) {
    app.controller("homeController", ["$scope", 
        function($scope) {
            $scope.contentFile = "<p class='center-align'>No ha sido cargado el reporte</p>";
            $scope.arrContentFile = [];
            $scope.isLoaded = false;
            $scope.resultProcess = "";
            let count = 0;
                        
            $scope.clearFile = function() {            
                let button = document.getElementById('btnFileUpload');
                button.value = "";
                $scope.fileArray = [];
                $scope.contentFile = "<p class='center-align'>No ha sido cargado el reporte</p>";
                $scope.isLoaded = false;
            };

            $scope.processFile = function() {                                
                let rowFormatted = [];
                let result = {};
                let stringIdx;
                let contentResultProcess =  "<div class=\"collection \">";
                for(let i = 0; i < $scope.arrContentFile.length; i++) {
                    if($scope.arrContentFile[i]) {                                                                                           
                        rowFormatted = parseRow($scope.arrContentFile[i]);
                        let timeWorking = getHoursWorking(rowFormatted);                                                                            
                        let collectMoney = getHowMoney(timeWorking);                         
                        stringIdx = rowFormatted.day + "";                        
                        if(result[stringIdx]) result[stringIdx] += collectMoney
                        else result[stringIdx] = collectMoney;                                                                                                                                                                            
                        count++;

                    }
                }  
                let total = 0;
                Object.keys(result).forEach(function(key) {
                    contentResultProcess += "<a href=\"#!\" style=\"color: #304ffe !important;\" class=\"collection-item \">Importe a la fecha 12/" + key +
                    "<span class=\"new badge red\" data-badge-caption=\"\">$" + result[key] +"</span>";
                    total+=result[key];
                });
                
                contentResultProcess += "<a href=\"#!\" style=\"color: #304ffe !important;\" class=\"collection-item \">El importe total es:" +
                "<span class=\"new badge red\" data-badge-caption=\"\">$" + total +"</span></div>";
                $scope.resultProcess = contentResultProcess;
            }
                        
            function getHowMoney(timeWorking) {
                const firstTwoHours = 300;
                const subsequentHours = 200;
                const amountByMinuteFirstTwoHours = parseFloat(firstTwoHours / 60);
                const amountByMinuteSubsequentHours = parseFloat(subsequentHours / 60);
                let amount;                                                                                
                if(timeWorking.hours < 2) {
                    amount = parseInt(timeWorking.hours) * firstTwoHours + parseInt(timeWorking.minutes) * amountByMinuteFirstTwoHours;
                }
                else {                 
                    amount = 2 * firstTwoHours;
                    timeWorking.hours -= 2;
                    amount += parseInt(timeWorking.hours) * subsequentHours;
                    amount += parseInt(timeWorking.minutes) * amountByMinuteSubsequentHours;
                }                
                return amount;
            }

            function getHoursAndMinutes(data) {
                let result = {minutes : "", hours : ""};
                if(data.length > 1) {
                    let arrStartHour = data.split(':');
                    result.hours = arrStartHour[0];
                    if(isInt(arrStartHour[1])) result.minutes = arrStartHour[1];
                    else result.minutes = arrStartHour[2] ? arrStartHour[2] : "";
                } else  result.hours = data;                
                return result;
            }

            function getHoursWorking(data) {                
                let result = {};
                let startHour;
                let startMinutes;
                let endHour;
                let endMinutes;                
                let resultStart = getHoursAndMinutes(data.startHour);
                startHour = resultStart.hours;
                startMinutes = resultStart.minutes == "" ? 0 : resultStart.minutes;
                let resultEnd = getHoursAndMinutes(data.endHour);
                endHour = resultEnd.hours;
                endMinutes = resultEnd.minutes == "" ? 0 : resultEnd.minutes ;
                let minutesWorking;                                
                if(endMinutes > startMinutes)
                    minutesWorking = parseInt(endMinutes) - parseInt(startMinutes);
                else {
                    if(startMinutes > endMinutes) {                        
                        startHour = parseInt(startHour) + 1;
                        minutesWorking = parseInt((60 - startMinutes)) + parseInt(endMinutes);                        
                    }else if(startMinutes == 0 && endMinutes == 0) minutesWorking = 0;                    
                }
                let hoursWorking = parseInt(endHour) - parseInt(startHour);
                result.hours = hoursWorking;
                result.minutes = minutesWorking;                
                return result;           
            }
            
            function getWorkShift(value) {
                let lengthValue = value.length;
                let result = {lengthIdx:0,  workShift: ""};
                for(let i = 0; i < lengthValue; i++) {
                    result.lengthIdx += 1;
                    let charToCheck = value.charAt(i);                                                                                
                    if(charToCheck == 'a' || charToCheck == 'p' || charToCheck == 'm') {                        
                        result.workShift += charToCheck;
                    }else if(isInt(charToCheck)) result.lengthIdx -=1; // esto es para no recortar caracteres numericos pues pertenecen a la hora
                }                                
                return result;
            }
          
            function getDayOrHour(value){
                let lengthData = value.data.length;
                let result = {lengthIdx:0, value:"", workShift: ""};
                let dataString = value.data;
                for(let i = 0; i < lengthData; i++) {
                    result.lengthIdx += 1;                    
                    let charToCheck = dataString.charAt(i);
                    if(isInt(charToCheck)) {                                                                 
                        result.value += charToCheck;
                    }else if(charToCheck == 'a' || charToCheck == 'p' || charToCheck == 'm') {                        
                        result.workShift += charToCheck;
                    } else if(value.source == "hour") {
                            result.value +=charToCheck;
                    }                
                }
                return result;
            }

            function parseRow(row) {
                let backRow = row;                                   
                let date = row.split('/',2);
                let lengthMonth = date[0].length;
                let month = date[0];                
                row = backRow.substring(lengthMonth+1);                
                date = row.split(':');                                
                let objResult =  getDayOrHour({data:date[0], source:"day"});
                let day = objResult.value;
                let workShift =objResult.workShift;
                row  = row.substring(objResult.lengthIdx +1);                                              
                if(!workShift) {                
                    date = row.split(' ');                    
                    let objWorkShift = getWorkShift(date[0]);
                    workShift = objWorkShift.workShift;                    
                    row = row.substring(objWorkShift.lengthIdx + 1);                    
                }                                   
                date = row.split('-');                
                objResult =  getDayOrHour({data:date[0], source:"hour"});
                let startHour = objResult.value;
                if(workShift == "") workShift = objResult.workShift;                                
                startHour = formatHour(startHour);                                
                let endHour = date[1];
                endHour = formatHour(endHour);
                let result = {month: month, day:day , workShift: workShift, startHour: startHour, endHour: endHour};                
                return result;
            }

            function formatHour(hour) {
                let cont = 0;
                let iterate = true;                
                while(iterate) {
                    if(isInt(hour.charAt(cont))) 
                        iterate = false;                                                                
                    cont++;
                }
                hour = hour.substring(cont-1).replace(/\s/g, ":");                       
                return hour;
            }
            
            function isInt(value) {
                let x;
                if (isNaN(value)) {
                    return false;
                }
                x = parseFloat(value);
                return (x | 0) === x;
            }
            
        }
    ]);
}(app));