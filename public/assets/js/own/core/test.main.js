let app = angular.module('testBBP', [ 'ngCookies', 'ngSanitize']);
app.config(
    function ($controllerProvider, $compileProvider, $provide) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|whatsapp|comgooglemapsurl|comgooglemaps|sms|tel):/);
        app.controllerProvider = $controllerProvider;
        app.compileProvider = $compileProvider;        
        app.provide = $provide;        
    });
