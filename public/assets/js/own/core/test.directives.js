(function(app) {   
    app.directive('ngBindHtmlCompile', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.ngBindHtmlCompile);
                }, function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                });
            }
        };
    }]);


    app.directive('dynamic', function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
                scope.$watch(attrs.dynamic, function (html) {
                    ele.html(html);
                    $compile(ele.contents())(scope);
                });
            }
        };
    });

    app.directive("selectNgFiles", function() {
        return {
          require: "ngModel",
          link: function postLink($scope,elem,attrs,ngModel) {          
            let input = elem[0]; // getting the target (input file), in this case just we have one            
            
            elem.on("change", function(e) { // when the element change we're gonna read the file
                if(input.files[0].type == "text/plain") {
                    let reader = new FileReader();
                    reader.onload = function () {
                        let text = reader.result;                    
                        let lines = text.split(/[\r\n]+/g); // tolerate both Windows and Unix linebreaks                                        
                        let textInline = '';                               
                        let arrRows = [];
                        lines.forEach(function(line) { 
                            textInline += "<p class='center-align'>" + line + "</p>";                        
                            arrRows.push(line);
                        });                                                
                        $scope.contentFile = textInline;   
                        $scope.isLoaded = true;
                        $scope.arrContentFile = arrRows;                 
                        if($scope.$$phase !== '$apply' && $scope.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    };                
                    reader.readAsText(input.files[0]);
                    let files = input.files;                    
                    ngModel.$setViewValue(files);                
                }else {                    
                    let arrError = [{name:"Error el archvo cargado no es uno de tipo txt."}];
                    ngModel.$setViewValue(arrError);                
                }
            });
          }
        };
    });

}(app));


